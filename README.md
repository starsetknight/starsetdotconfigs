# Starsetdotconfigs

## Description
My dotconfigs for my setup.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Support
I will not be providing support. These configs are mainly for my personal use, if you want to use any or all of them feel free, but you are on your own.

## Contributing
If you want to do a merge request, go ahead, I will accept if there is a reason that I find valid to it existing (in other words I probably won't be accepting any merge requests).
## License
This is Licensed under the GNU General Public Licence Version 3.
